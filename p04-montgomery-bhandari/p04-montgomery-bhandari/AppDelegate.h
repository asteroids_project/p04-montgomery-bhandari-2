//
//  AppDelegate.h
//  p04-montgomery-bhandari
//
//  Created by Montgomery Chelsea on 2/20/16.
//  Copyright © 2016 SUNY_Binghamton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


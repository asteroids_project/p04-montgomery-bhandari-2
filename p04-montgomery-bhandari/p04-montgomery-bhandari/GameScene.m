//
//  GameScene.m
//  p04-montgomery-bhandari
//
//  Created by Montgomery Chelsea on 2/20/16.
//  Copyright (c) 2016 SUNY_Binghamton. All rights reserved.
//

#import "GameScene.h"

//Categories
static NSString* rocket_category = @"rocket";
static NSString* missle_category = @"missle";
static NSString* asteroid_category = @"asteroid";

static const uint32_t asteroidCategory = 0x1 << 1;
static const uint32_t missleCategory = 0x1 << 0;
static const uint32_t rocketCategory = 0x1 << 2;

@interface GameScene ()
@property (nonatomic, strong) SKSpriteNode *rocket;
@property (nonatomic, strong) SKSpriteNode *left_button;
@property (nonatomic, strong) SKSpriteNode *right_button;
@property (nonatomic, strong) SKSpriteNode *fire_button;
@property (nonatomic) NSTimeInterval lastSpawnTimeInterval;
@property (nonatomic) NSTimeInterval lastUpdateTimeInterval;
@end

@implementation GameScene

Boolean left = true;



-(id)initWithSize:(CGSize)size
{
    if(self = [super initWithSize:size])
    {
        
        NSLog(@"size: %@", NSStringFromCGSize(size));
        
        self.backgroundColor= [SKColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
        
        self.rocket = [SKSpriteNode spriteNodeWithImageNamed:@"rocket.png"];
        self.rocket.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
        [self addChild:self.rocket];
        
        self.physicsWorld.gravity = CGVectorMake(0,0);
        self.physicsWorld.contactDelegate = self;
        
        
        //Add buttons to screen
        self.left_button = [SKSpriteNode spriteNodeWithImageNamed:@"arrow.png"];
        self.left_button.position = CGPointMake(self.frame.size.width/3-60, 100);
        self.left_button.zPosition = 1;
        [self addChild:self.left_button];
        
        self.right_button = [SKSpriteNode spriteNodeWithImageNamed:@"arrow_right.png"];
        self.right_button.position = CGPointMake(((self.frame.size.width/3)*2) - 60, 100);
        self.right_button.zPosition = 1;
        [self addChild:self.right_button];
        
        self.fire_button = [SKSpriteNode spriteNodeWithImageNamed:@"fire_button.png"];
        self.fire_button.position = CGPointMake(self.frame.size.width - 60, 100);
        self.fire_button.zPosition = 1;
        [self addChild:self.fire_button];
        
        
        

        
    }
    
    return self;
}

//timing methods
-(void)updateWithTimeSinceLastUpdate:(CFTimeInterval)timeSinceLast
{
    self.lastSpawnTimeInterval +=timeSinceLast;
    if(self.lastSpawnTimeInterval >3)
    {
        self.lastSpawnTimeInterval = 0;
        [self addAsteroid];
    }
}

//Called each frame by sprite kit
-(void)update:(NSTimeInterval)currentTime
{
    CFTimeInterval timeSinceLast = currentTime - self.lastUpdateTimeInterval;
    self.lastUpdateTimeInterval= currentTime;
    
    if(timeSinceLast>1){
        timeSinceLast = 1.0/60.0;
        self.lastUpdateTimeInterval = currentTime;
    }
    
    [self updateWithTimeSinceLastUpdate:timeSinceLast];
}


//Function to add asteroid
-(void)addAsteroid
{
    NSLog(@"Adding asteroid");
    
    //Create sprite
    SKSpriteNode *asteroid = [SKSpriteNode spriteNodeWithImageNamed:@"asteroid.png"];
    
    asteroid.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:asteroid.size];
    asteroid.physicsBody.dynamic = YES;
    asteroid.physicsBody.categoryBitMask = asteroidCategory;
    asteroid.physicsBody.contactTestBitMask = missleCategory;
    asteroid.physicsBody.collisionBitMask = 0;
    
    if(left)
    {
    
        //Determine where to spawn
        int minY = asteroid.size.height/2;
        int maxY = self.frame.size.height - asteroid.size.height/2;
        int rangeY = maxY-minY;
        int actualY = (arc4random()%rangeY) + minY;
    
        //Create off screen
        asteroid.position = CGPointMake(self.frame.size.width + asteroid.size.width/2, actualY);
        [self addChild:asteroid];
    
        //Determine speed of asteroid
        /*int minDuration = 2.0;
        int maxDuration = 4.0;
        int rangeDuration = maxDuration - minDuration;
        int actualDuration = (arc4random()% rangeDuration) + minDuration;*/
        int actualDuration = 6;
        
        //Get y skew
        int ySkew =(arc4random_uniform(1000) - 500);
    
        //Create the actions
        SKAction *actionMove = [SKAction moveTo:CGPointMake(-asteroid.size.width/2, actualY + ySkew) duration:actualDuration];
        SKAction *actionMoveDone = [SKAction removeFromParent];
        [asteroid runAction:[SKAction sequence:@[actionMove, actionMoveDone]]];
        
        left = false;
    }
    
    else
    {
        //Determine where to spawn
        int minX = asteroid.size.width/2;
        int maxX = self.frame.size.width - asteroid.size.width/2;
        int rangeX = maxX-minX;
        int actualX = (arc4random()%rangeX) + minX;
        
        //Create off screen
        asteroid.position = CGPointMake(actualX, self.frame.size.height + asteroid.size.height/2);
        [self addChild:asteroid];
        
        //Determine speed of asteroid
        /*int minDuration = 2.0;
        int maxDuration = 4.0;
        int rangeDuration = maxDuration - minDuration;
        int actualDuration = (arc4random()% rangeDuration) + minDuration;*/
        int actualDuration = 10;
        
        //Get x skew
        int xSkew = (arc4random_uniform(1000) - 500);
        
        //Create the actions
        SKAction *actionMove = [SKAction moveTo:CGPointMake(actualX + xSkew, -asteroid.size.height/2) duration:actualDuration];
        SKAction *actionMoveDone = [SKAction removeFromParent];
        [asteroid runAction:[SKAction sequence:@[actionMove, actionMoveDone]]];
        
        left = true;
    }
    
}

@end
